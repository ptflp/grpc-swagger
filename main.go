package main

import "gitlab.com/ptflp/grpc-swagger/server"

func main() {
	g := server.New()
	g.Start()
}