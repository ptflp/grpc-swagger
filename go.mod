module gitlab.com/ptflp/grpc-swagger

go 1.14

require (
	github.com/golang/protobuf v1.4.1
	github.com/grpc-ecosystem/grpc-gateway v1.15.0
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0 // indirect
)
